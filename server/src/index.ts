import * as Koa from 'koa'
import * as path from 'path'
import * as serve from 'koa-static'
import * as winston from 'winston'
import * as logger from 'koa-logger'
const historyFallback = require('koa2-history-api-fallback')
const compress = require('koa-compress')

const app = new Koa();

// Logger
app.use(logger())

app.use(historyFallback())

app.use(compress({
    threshold: 2048,
    flush: require('zlib').Z_SYNC_FLUSH
  }))

app.use(serve(path.join(__dirname, '../public')))

app.on('info', (error) => {
    winston.log('error', error)
})

const PORT = process.env.PORT || 3005
app.listen(PORT, () => console.log(`Server listening on port ${PORT}`))