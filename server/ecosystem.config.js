module.exports = {
  apps: [{
    name: 'web-client-service',
    script: 'server/bin/www',
    env: {
      NODE_ENV: 'development',
    },
    env_production: {
      NODE_ENV: 'production',
    },
  }],
};
