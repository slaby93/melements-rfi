/* globals workbox, self */

workbox.core.setCacheNameDetails({
  prefix: 'mElements',
  suffix: 'v1',
  precache: 'mElements-precache',
  runtime: 'mElements-runtime-cache',
})

workbox.precaching.precacheAndRoute(self.__precacheManifest)