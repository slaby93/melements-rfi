import * as React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import styled from 'styled-components'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'
import useRepos from './useRepos'
import breakpoints from './../../consts/breakpoints'
import SearchResultItem from './SearchResultItem'

const SearchResults = styled(React.memo(({ className, language, name, moveToHomepage }) => {
  // if there is no filter 
  // ( eg. someone removed data from redux or clearead localStorage )
  if (!language) return moveToHomepage()

  const { results, hasNextPage, isFetching, fetchEvents } = useRepos()
  React.useEffect(() => { // same as componentDidMount
    fetchEvents(language, name)
  }, [])

  return (
    <div className={className}>
      <ResultsWrapper>
        {results && (
          results.map(item => <SearchResultItem key={item.get('id')} item={item} />)
        )}
      </ResultsWrapper>
      {results && !results.size && <Grid container justify="center" alignItems="center">
        <Typography variant="h4">No results</Typography>
      </Grid>}

      <Grid className="search-resulsts__leftover-wrapper" container justify="center" alignItems="center">
        {hasNextPage && !isFetching && <Button
        data-automation-id="search__load-more-button" 
        onClick={() => fetchEvents(language, name)}>
          <Typography variant="h6" gutterBottom color="primary" >Load More</Typography>
        </Button>}
        {isFetching && <CircularProgress />}
      </Grid>
    </div>
  )
}))`
  .search-resulsts__leftover-wrapper { margin-bottom: 10px; }
`

const ResultsWrapper = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: ${breakpoints.small}px) {
    display: grid;
    grid-template-columns: 30% 30% 30%;
    grid-template-rows: auto;
    justify-content: space-between;
  }
`

const mapStateToProps = (state, props) => {
  const id = new URLSearchParams(props.location.search).get('id')
  let filter = state.getIn(['filters', 'list']).filter(item => item.get('id') === id)
  filter = filter && filter.get(0)

  return {
    language: filter ? filter.get('language') : null,
    name: filter ? filter.get('name') : null,
  }
}
const mapDispatchToProps = dispatch => ({ moveToHomepage: () => dispatch(push('/')) })

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults)