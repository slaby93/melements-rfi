import * as React from 'react'
import { shallow } from 'enzyme'
import { fromJS } from 'immutable'
import { SearchResultItem, StyledSearchResultItem } from './../SearchResultItem'

describe('<FullScreenLoader />', () => {
  it('should render component Style', () => {
    const component = shallow(<StyledSearchResultItem />)
    expect(component).toMatchSnapshot()
  })
})