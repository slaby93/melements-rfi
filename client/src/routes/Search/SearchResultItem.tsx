import * as React from 'react'
import styled from 'styled-components'
import Chip from '@material-ui/core/Chip'
import Typography from '@material-ui/core/Typography'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import SVGIcon from 'components/special/SVGIcon'
import DownArrow from 'resources/icons/down-arrow.svg'

interface ISearchResultItemProps {
  className: string, 
  item?: Map<string, any>
}

export const SearchResultItem = ({ className, item }: ISearchResultItemProps) => {
  if (!item) { return null }
  const totalSize = React.useMemo(() => item.getIn(['languages', 'totalSize']), [item])

  return (
    <a data-automation-id="search-result-item" className={className} href={item.get('url')} target="_blank">
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<SVGIcon
                        data-automation-id="search-result-item__open-item"
                        onClick={React.useCallback((event: React.MouseEvent<HTMLInputElement>) => event.preventDefault(), [])} 
                        src={DownArrow} />}>
          <ContentWrapper>
            <TextWrapper>
              <Typography>{item.get('name')}-{item.get('description')}</Typography>
            </TextWrapper>
            <Chip
              color="secondary"
              className="search__stargazers-chip"
              label={`${item.getIn(['stargazers', 'totalCount'])} Stars`} />

          </ContentWrapper>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <List>
            {
              item.getIn(['languages', 'edges']).map(language => {
                const { name, percentage } = React.useMemo(() => {
                  const name = language.getIn(['node', 'name'])
                  const languageSize = language.get('size')
                  const percentage = ((languageSize / totalSize) * 100).toFixed(1)
                  return { name, percentage }
                }, [language])

                return (
                  <li key={name}>
                    <Chip 
                      color="primary" 
                      label={`${name} ${percentage}%`}
                      data-automation-id="search-result-item__language-chip"
                      />
                  </li>
                )
              })
            }
          </List>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </a>
  )
}

export const StyledSearchResultItem = styled(SearchResultItem)`
  position: relative;
  display: block;
  margin: 10px;
  text-decoration: none;

  .search-result-item__expansion-panel {
    position: relative;
  }
`

const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  width: 100%;

  & > li {
    margin-right: 5px;
    margin-bottom: 5px;
  }
`

const TextWrapper = styled.div`
  display: flex;
`

const ContentWrapper = styled.div`
  padding-right: 0 !important;

  .search__stargazers-chip {
    margin-top: 10px;
  }
`

export default React.memo(StyledSearchResultItem)