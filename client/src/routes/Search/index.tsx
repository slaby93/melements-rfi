import React, { lazy, Suspense } from 'react'
import FullScreenLoader from 'components/special/FullScreenLoader';

const View = lazy(() => import(/* webpackChunkName: "Search" */'./Search'))
export default (props: any) => (
  <Suspense
    fallback={<FullScreenLoader />}>
    <View {...props} />
  </Suspense>
)