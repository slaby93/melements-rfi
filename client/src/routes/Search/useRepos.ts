import { useState } from 'react'
import { subYears, format } from 'date-fns'
import github from './../../services/github'

const PAGE_SIZE = 20

export default () => {
  const [results, setResults] = useState(null)
  const [isFetching, setIsFetching] = useState(false)
  const [{ endCursor, hasNextPage }, setPageInfo] = useState({ endCursor: undefined, hasNextPage: null })

  const fetchEvents = async (language: string, name:string) => {
    setIsFetching(true)
    const yearAgo = format(subYears(new Date(), 1), 'yyyy-MM-dd')
    const response = await github.get({ size: PAGE_SIZE, minPushedDate: yearAgo, language, name, endCursor })
    const pageInfo = response && response.getIn(['data', 'search', 'pageInfo'])
    setPageInfo({ endCursor: pageInfo.get('endCursor'), hasNextPage: pageInfo.get('hasNextPage') })
    const parsedResponse = response && response.getIn(['data', 'search', 'nodes'])
    setResults(results ? results.concat(parsedResponse) : parsedResponse)
    setIsFetching(false)
  }

  const reset = () => {
    setResults(null)
  }

  return { results, hasNextPage, isFetching, fetchEvents, reset }
}
