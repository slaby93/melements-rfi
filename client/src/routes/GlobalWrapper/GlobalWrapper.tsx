import * as React from 'react'
import styled, { ThemeProvider } from 'styled-components'
import Header from './Header'
import theme from 'themes/default'
import GlobalStyle from './../../GlobalStyle'
import breakpoints from './../../consts/breakpoints'

interface IGlobalWrapperProps {
  className: string
}

export class GlobalWrapper extends React.Component<IGlobalWrapperProps> {
  render() {
    const { className, children } = this.props
    
    return (
      <ThemeProvider theme={theme}>
        <div className={className}>
          <GlobalStyle />
          <Header />
          <Body>
            {children}
          </Body>
        </div>
      </ThemeProvider>
    )
  }
}

const Body = styled.div`
  flex-grow: 1;
  max-height: 100vh;
  margin: 10px;

  @media (min-width: ${breakpoints.small}px) {
    display: flex;
    justify-content: center;

    & > div {
      flex-grow: 1;
      max-width: 1000px;
    }
  }
`


const Styled = styled(GlobalWrapper)`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  height: 100%;
`


export default Styled;
