import * as React from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import SVGIcon from 'components/special/SVGIcon'
import OctocatIcon from 'resources/icons/octocat.svg'
import LeftArrowIcon from 'resources/icons/left-arrow.svg'

interface IRoute {
  [index: string]: { title: string, back: string | null }
}

const ROUTES_HEADERS: IRoute = {
  '/': {
    title: 'search favourites',
    back: null
  },
  '/search': {
    title: 'search results',
    back: '/'
  }
}

const Header = React.memo(({ location }) => {

  const { title, back } = React.useMemo(() => {
    return ROUTES_HEADERS[location.pathname]
  }, [location.pathname])

  return (
    <AppBar position="static" color="primary">
      <Toolbar>
        <HeaderContent>
          {back && (
            <Link to={back}>
              <LeftArrow src={LeftArrowIcon} />
            </Link>
          )}
          <Typography variant="h5">
            {title}
          </Typography>
          <Octocat src={OctocatIcon} size={70} />
        </HeaderContent>
      </Toolbar>
    </AppBar>
  )
})

const HeaderContent = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
`

const Octocat = styled(SVGIcon)`
  margin-left: auto;
`

const LeftArrow = styled(SVGIcon)`
  margin-right: 10px;
`

export default withRouter(Header)