import * as React from 'react'
import {
  Switch,
  Redirect,
  Route
} from 'react-router-dom'
import GlobalWrapper from './GlobalWrapper'
import Homepage from './Homepage'
import Search from './Search'


export default () => {
  return (
    <GlobalWrapper>
      <Switch>
        <Route exact path="/" render={Homepage} />
        <Route path="/search" render={Search} />
        <Redirect to="/" />
      </Switch>
    </GlobalWrapper>
  )
}
