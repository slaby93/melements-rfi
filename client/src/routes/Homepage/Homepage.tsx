import * as React from 'react'
import styled from 'styled-components'
import CreateFilterForm from './CreateFilterForm'
import FilterList from './FilterList'

interface IHomepageProps {
  className: string
}

export const Homepage = styled(React.memo(({ className }: IHomepageProps) => {

  return (
    <div className={className}>
      <CreateFilterForm/>
      <FilterList />
    </div>
  )
}))`
  display: flex;
  flex-direction: column;
  max-height: 100vh;
`

export default Homepage
