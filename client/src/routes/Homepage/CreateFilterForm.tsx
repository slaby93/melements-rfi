import * as React from 'react'
import { connect } from 'react-redux'
import { Form, Field } from 'react-final-form'
import styled from 'styled-components'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import { CREATE_FILTER } from 'ducks/filters'
import IFilter from 'interfaces/Filter'
import breakpoints from './../../consts/breakpoints'

const SELECT_OPTIONS = {
  "javascript": "JavaScript",
  "java": "Java",
  "python": "Python",
  "ruby": "Ruby",
  "go": "Go",
  "bash": "Shell",
  "typescript": "TypeScript",
  "groovy": "Groovy",
  "scala": "Scala",
  "kotlin": "Kotlin",
  "php": "PHP",
  "cpp": "C++",
  "c": "C"
}

interface ICreateFilterFormProps {
  className: string,
  addEvent: (data: { name: string, language: string }) => void
}

export const CreateFilterForm = styled(React.memo(({ className, addEvent }: ICreateFilterFormProps) => {
  // nice usage of useCallback
  // In React before hooks, we would need to create Class PureComponent and bind it
  // either in contructor, or with proposal-class-properties to avoid rerendering
  const onSubmit = React.useCallback((data: IFilter, formApi) => {
    formApi.reset()
    addEvent(data)
  }, [])
  // I know this may seems strange, belive me it's still for me, but apparently this is how
  // we supose to write it? Usually I store such functions in utils file where I can test them, but 
  // this is so simple function
  const nameFieldValidator = React.useCallback(value => !value && 'Field is required!', [])

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{
        language: Object.keys(SELECT_OPTIONS)[0]
      }}
    >
      {
        formApi => {
          return (
            <form data-automation-id="create-filter-form" onSubmit={formApi.handleSubmit} className={className}>
              <Card>
                <CardContent>
                  <FieldsWrapper>
                    <Field name="name" validate={nameFieldValidator}>
                      {
                        ({ input, meta }) => (
                          <TextFieldWrapper>
                            <TextField
                              {...input}
                              id="standard-name"
                              label="Repo name or description"
                              margin="normal"
                              error={meta.touched && !!meta.error}
                            />
                            {meta.error && meta.touched && <Typography color="error">{meta.error}</Typography>}
                          </TextFieldWrapper>
                        )
                      }
                    </Field>
                    <Field name="language">
                      {
                        ({ input }) => (
                          <TextField
                            {...input}
                            select
                            id="standard-name"
                            label="Language"
                            margin="normal"
                          >
                            {
                              SELECT_OPTIONS
                              |> (options => Object.entries(options))
                              |> (options => options.map((([key, translation]) => (
                                <MenuItem key={key} value={key}>
                                  <Typography>{translation}</Typography>
                                </MenuItem>
                              ))))
                            }
                          </TextField>
                        )
                      }
                    </Field>
                    <Button
                      data-automation-id="create-filter-form__add-to-list-button"
                      disabled={!formApi.valid}
                      className="create-filter__submit-button"
                      type="submit"
                      color="primary">
                      + ADD TO LIST
                    </Button>
                  </FieldsWrapper>
                </CardContent>
              </Card>
            </form>
          )
        }
      }
    </Form>
  )
}))`
  display: flex;
  flex-direction: column;
  min-height: fit-content;

  .create-filter {

    &__submit-button {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      margin-top: 10px;
      font-weight: bold;
    }
  }
`

const FieldsWrapper = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: ${breakpoints.small}px) {
    flex-direction: row;
    justify-content: space-evenly;

    & > * {
      width: 201px;
    }
  }
`

const TextFieldWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const mapDispatchToProps = dispatch => (
  {
    addEvent: (data: IFilter) => dispatch(CREATE_FILTER(data))
  }
)

export default connect(null, mapDispatchToProps)(CreateFilterForm)
