import * as React from 'react'
import styled from 'styled-components'
import Swipeout from 'rc-swipeout'
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import { Button } from '@material-ui/core'
import Chip from '@material-ui/core/Chip'
import SVGIcon from 'components/special/SVGIcon'
import TrashIcon from 'resources/icons/trash.svg'
import breakpoints from './../../consts/breakpoints'

const FilterItem = styled(({ className, filter, onDelete }) => {

  const onRemove = React.useCallback((event?: React.MouseEvent) => {
    event && event.preventDefault()
    onDelete(filter.get('id'))
  }, [])

  return (
    <div 
      data-automation-id="filter-item" 
      className={className}>
      <Swipeout
        right={[
          {
            text: 'Delete',
            onPress: onRemove,
            style: { backgroundColor: '#2196f3', color: 'white' }
          }
        ]}
      >
        <Card>
          <Link to={`/search?id=${filter.get('id')}`}>
            <CardContent>
              <Typography className="filter-list-item__title" variant="h5">
                {filter.get('name')}
              </Typography>
              <MinorContentWrapper>
                <Chip color="primary" label={filter.get('language')} />
                <Button
                  data-automation-id="filter-list-item__delete-button"
                  onClick={onRemove}>
                  <SVGIcon src={TrashIcon} />
                  <span>Delete</span>
                </Button>
              </MinorContentWrapper>
            </CardContent>
          </Link>
        </Card>
      </Swipeout>
    </div>
  )
})`
    margin: 10px 0;
    text-transform: capitalize;

    .filter-list-item {
        &__title {
            margin-bottom: 10px;
        }
    }

    a {
      text-decoration: none;
    }

    .rc-swipeout {
      overflow: visible;
    }
`

const MinorContentWrapper = styled.div`
  display: flex;
  justify-content: space-between;

  ${SVGIcon} {
    margin-right: 5px;
  }

  @media (max-width: ${breakpoints.small}px) {
    & > button {
      display:none;
    }
  }
`

export default FilterItem