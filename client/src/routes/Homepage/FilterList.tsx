import * as React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import IFilter from 'interfaces/Filter'
import { REMOVE_FILTER } from './../../ducks/filters'
import breakpoints from './../../consts/breakpoints'
import FilterItem from './FilterItem'

interface IFilterListProps {
  className: string,
  filters: [IFilter],
  removeFilter: (id: string) => void
}

const FilterList = styled(React.memo(({ filters, removeFilter, className }: IFilterListProps) => {
    return (
      <div id="filter-list" className={className}>
        {
          filters.map(filter => <FilterItem onDelete={removeFilter} key={filter.get('id')} filter={filter} />)
        }
      </div>
    )
  }))`
  display: flex;
  flex-direction: column;

  @media (min-width: ${breakpoints.small}px) {
    display: grid;
    grid-template-columns: 30% 30% 30%;
    grid-template-rows: auto;
    justify-content: space-between;
  }
`

const mapStateToProps = state => ({
  filters: state.getIn(['filters', 'list']),
  state
})

const mapDispatchToProps = dispatch => ({
  removeFilter: (id: string) => dispatch(REMOVE_FILTER({ id }))
})

export default connect(mapStateToProps, mapDispatchToProps)(FilterList)