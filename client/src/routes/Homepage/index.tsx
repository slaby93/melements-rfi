import React, { lazy, Suspense } from 'react'
import FullScreenLoader from 'components/special/FullScreenLoader';

const View = lazy(() => import(/* webpackChunkName: "Homepage" */'./Homepage'))
export default (props: any) => (
  <Suspense
    fallback={<FullScreenLoader />}>
    <View {...props} />
  </Suspense>
)