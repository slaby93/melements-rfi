
import store from 'store'
import { API_GRAPHQL_URL } from 'consts/environment'
import { bindActionCreators } from 'redux'
import request from 'utils/request'
import { fromJS } from 'immutable'

const MIN_STARS = 10
const TYPE = 'REPOSITORY'

interface IGetParams {
  language: string,
  name: string,
  minPushedDate: string,
  size: number,
  offset?: number,
  endCursor?: string
}

function get({ language, name, minPushedDate, size, endCursor }: IGetParams) {
  return async () => {
    const response = await request(API_GRAPHQL_URL, {
      method: 'POST',
      body: JSON.stringify({
        query: `
        {
          search(
            query: "${name} stars:>${MIN_STARS} 
            pushed:>${minPushedDate} 
            language:${language}", 
            first: ${size},
            ${endCursor ? `after: "${endCursor}"` : ''}
            type: ${TYPE}) 
          {
            pageInfo {
              endCursor
              hasNextPage
            }
            nodes {
                ... on Repository {
                  name
                  description
                  url
                  id
                  stargazers {
                    totalCount
                  }
                  languages(first: 100) {
                    totalSize
                    edges {
                      size
                      node {
                        color
                        name
                    }
                  }
                }
              }
            }
          }
        }
        
        `
      })
    })

    return fromJS(response)
  }
}

export default bindActionCreators({
  get,
}, store.dispatch)
