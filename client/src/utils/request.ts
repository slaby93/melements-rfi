import localStorageManager from 'utils/localStorageManager'
import { GITHUB_TOKEN } from 'consts/environment'

export const METHODS = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
  PUT: 'PUT'
}

/**
 * request is just a wrapper around fetch.
 */
export default async (url: string, options = {}) => {
  const {
    method = METHODS.GET, body, headers, ...rest
  } = options

  const authToken = localStorageManager.get('id_token')

  const response = await fetch(url, {
    ...rest,
    body: method === METHODS.GET ? null : body,
    method,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ${GITHUB_TOKEN}`,
      ...headers,
    }
  })
  
  const parsedResponse = await response.json()

  if( parsedResponse.errors ) {
    throw parsedResponse
  }

  return parsedResponse
}
