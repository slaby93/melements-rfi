class PushNotification {
  private static _instance: PushNotification;

  private constructor() { }

  defaultNotificationOptions = {
    body: 'Hello from mElements! :)',
    icon: '/icons/shoesx128.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: 1
    },
  }

  static STATUS = {
    GRANTED: 'granted',
    DENIED: 'denied',
  }

  isInstalled = () => {
    const status = Notification.permission

    return status === PushNotification.STATUS.GRANTED

  }

  install = () => new Promise((resolve) => {
    Notification.requestPermission(status => resolve(status));
  })

  displayNotification = async (title:string, customOptions?: {
    body?: string
  }) => {
    if (Notification.permission !== 'granted') {
      return
    }

    const reg: any = await navigator.serviceWorker.getRegistration()
    
    if (!reg) return

    reg.showNotification(title || this.defaultNotificationOptions.title, {
      ...this.defaultNotificationOptions,
      ...customOptions
    });
  }

  public static get Instance() {
    // Do you need arguments? Make it a regular method instead.
    return this._instance || (this._instance = new this());
  }
}

export default PushNotification.Instance;
