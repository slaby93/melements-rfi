import { Set, Map } from 'immutable'

/**
 * This method helps to pick just subset of properties from Map
 * https://github.com/facebook/immutable-js/wiki/Predicates
 */
Map.prototype.keyIn = (...keys: Array<string>) => {
  const keySet = Set(keys);
  return function (v: any, k: string) {
    return keySet.has(k);
  }
}