class InstallPWA {
  event: any = null
  canInstall: boolean = false

  installHook = () => {
    window.addEventListener('beforeinstallprompt', async (event) => {
      event.preventDefault()
      this.event = event
      this.canInstall = true
    });
  }

  install = async () => {
    if (!this.event || !this.canInstall) { return }
    this.event.prompt()

    const choiceResult = await this.event.userChoice
    if (choiceResult.outcome === 'accepted') {
      console.log('User accepted the A2HS prompt')
    } else {
      console.log('User dismissed the A2HS prompt')
    }
  }

  // function addToHomescreen() {
  //   window.addEventListener('beforeinstallprompt', async (event) => {
  //     event.preventDefault()
  //     event.prompt()

  //     const choiceResult = await event.userChoice
  //     if (choiceResult.outcome === 'accepted') {
  //       console.log('User accepted the A2HS prompt')
  //     } else {
  //       console.log('User dismissed the A2HS prompt')
  //     }
  //   });
  // }
}

export default new InstallPWA()