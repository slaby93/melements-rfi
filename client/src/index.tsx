import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { MuiThemeProvider } from '@material-ui/core/styles'
import 'rc-swipeout/assets/index.css'
import customMUITheme from './themes/custom-mui-theme'
import store, { history } from './store/store'
import Router from './routes/router'
import './utils/Immutable'
import installPWA from './utils/installPWA'

removeInitialLoader();
'serviceWorker' in navigator && initializePWA()

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={customMUITheme}>
      <ConnectedRouter history={history}>
        <Router />
      </ConnectedRouter>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('app'),
);

function removeInitialLoader() {
  const element = document.getElementById('loader');
  element && element.remove();
}

function installServiceWorker() {
  return new Promise((resolve) => {
    window.addEventListener('load', async () => {
      try {
        const registration = await navigator.serviceWorker.register('/workbox-sw.js');
        console.info('ServiceWorker registration successful with scope: ', registration.scope);
        resolve(registration)
      } catch (err) {
        console.error('ServiceWorker registration failed: ', err);
      }
    });
  })
}


async function initializePWA() {
  await installServiceWorker();
  installPWA.installHook()
}