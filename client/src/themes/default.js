
export default {
  color: {
    white: 'white',
    darkWhite: '#f9f9f9',
    gray: '#efefef',
    darkPrimary: '#455A64',
    defaultPrimary: '#607D8B',
    lightPrimary: '#CFD8DC',
    textPrimary: '#FFFFFF',
    accent: '#009688',
    primaryText: '#212121',
    secondaryText: '#757575',
    divider: '#BDBDBD',
  },
  components: {
    header: {
      height: 70,
    },
  },
};

