import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: { main: '#55c4f5'},
    secondary: { main: '#fbe1b9' }
  }
})

export default theme