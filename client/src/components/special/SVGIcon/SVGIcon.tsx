import React from 'react';
import styled, { css } from 'styled-components';

interface IThemeProps {
  size?: number,
  width: 20,
  height: 20,
  color?: string
}

interface IIconProp extends IThemeProps {
  src: string,
  className?: string,
  size?: number,
};

export const Icon = styled(class extends React.PureComponent<IIconProp> {

  translateIcon(icon: string): string {
    const result = icon
      .substr(1, icon.length - 1) // takes values between ""
      .replace(/%3C/g, '<')
      .replace(/%3E/g, '>')
      .replace(/%23/g, '#')
      .replace(/"/g, '')

    return result.substr(result.search(/<svg/g))
  }

  render() {
    const {
      src, className, size, ...props
    } = this.props
    const translatedIcon = this.translateIcon(src)

    return (
      <div
        className={className}
        dangerouslySetInnerHTML={{ __html: translatedIcon }}
        {...props}
      />
    )
  }
})`
  height: ${({ height }: IThemeProps) => height || 20}px;
  width: ${({ width }: IThemeProps) => width || 20}px;
  
  ${(({ size }: IThemeProps) => size && css`
      --size: ${size}px;
      height: var(--size);
      width: var(--size);
  `)}  

  svg {
    max-width: 100%;
    min-width: 100%;
    
    max-height: 100%;
    min-height: 100%;

    ${({ color }: IThemeProps) => {

      return color && css`
        fill: ${color};
      `
    }}
  }
`

export default Icon;
