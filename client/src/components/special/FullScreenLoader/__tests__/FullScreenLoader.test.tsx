import * as React from 'react'
import { shallow } from 'enzyme'
import Styled,{ FullScreenLoader } from './../FullScreenLoader'

describe('<FullScreenLoader />', () => {
  it('should render Styled component', () => {
    const component = shallow(<Styled />)
    expect(component).toMatchSnapshot()
  })

  it('should render HTML structure', () => {
    const component = shallow(<FullScreenLoader />)
    expect(component).toMatchSnapshot()
  })
})