import * as React from 'react'
import styled from 'styled-components'
import PushNotification from './../../utils/PushNotifications'
import SVGIcon from 'components/special/SVGIcon'
import BellIcon from 'resources/icons/bell.svg'

interface IProps {
  className: string,
  onFinished: () => void,
}

export const AskForNotificationPremission = styled(class extends React.PureComponent<IProps> {

  onAccept = async () => {
    const { onFinished } = this.props
    await PushNotification.install()
    onFinished()

  }

  onReject = () => {
    const { onFinished } = this.props
    onFinished()
  }

  render() {
    const { className } = this.props

    return (
      <div className={className}>
        <ContentWrapper>
          <SVGIcon size={100} src={BellIcon} />
          <h1>Please Turn On Notifications</h1>
          <SubTitle>
            This way, you and your friends will see messages instantly on your device
          </SubTitle>
        </ContentWrapper>
        <ButtonsWrapper>
          <div
            onClick={this.onAccept}
          >ok</div>
          <div onClick={this.onReject}>not now</div>
        </ButtonsWrapper>
      </div>
    )
  }
})`
  position: sticky;
  width: 100vw;
  height: 100vh;
  background-color: white;
  z-index: 10;
  padding: 30px 10%;
  box-sizing: border-box;
  text-align: center;
  justify-content: space-between;

  display: flex;
  flex-direction: column;

  ${SVGIcon} {
    margin-bottom: 40px;
  }
`

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const SubTitle = styled.p`
  color: darkgray;
`

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
`

export default AskForNotificationPremission