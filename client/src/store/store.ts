import { createStore, applyMiddleware, compose } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import thunk from 'redux-thunk';
import { combineReducers } from 'redux-immutable';
import reducers from '../ducks';

export const history = createHistory();
const routerMiddlewareInstance = routerMiddleware(history);

const combinedReducers = combineReducers({
  ...reducers,
  router: routerReducer,
})

const composeEnhancers = typeof window === 'object'
  && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) 
  : compose

const enhancer = composeEnhancers(applyMiddleware(routerMiddlewareInstance, thunk))
const store = createStore(combinedReducers, enhancer)

export default store;
