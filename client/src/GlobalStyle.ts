import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900');
@import url('https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900');

  html, body, #app {
    scroll-behavior: smooth;
    margin: 0;
    min-height: 100vh;
    display: flex;
    flex-grow: 1;
    max-width: 100vw;
  }

  html, button {
    font-family: 'Montserrat','Lato', sans-serif;
    -webkit-font-smoothing: antialiased;
  }

  body {
    background-color: #f5f5f5;
  }
`
