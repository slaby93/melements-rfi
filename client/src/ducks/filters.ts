import { createAction, handleActions } from 'redux-actions'
import { fromJS, List } from 'immutable'
import uuidv4 from 'uuid/v4'
import localStorageManager from 'utils/localStorageManager'

export const CREATE_FILTER = createAction('EVENT/CREATE')
export const REMOVE_FILTER = createAction('EVENT/REOMVE')
const LOCAL_STORAGE_KEY = 'filters-state'


const dataFromLocalStorage = localStorageManager.get(LOCAL_STORAGE_KEY)
const defaultState = dataFromLocalStorage && fromJS(JSON.parse(dataFromLocalStorage)) || fromJS({
    list: new List()
})

const reducer = handleActions({
    [CREATE_FILTER]: (state, { payload: { name, language } }) => {
        const newState = state.update('list', filters => filters.push(fromJS({ name, language, id: uuidv4() })))
        localStorageManager.set(LOCAL_STORAGE_KEY, JSON.stringify(newState.toJS()))
        return newState
    },
    [REMOVE_FILTER]: (state, { payload: { id } }) => {
        const newState = state.update('list', filters => filters.filter(item => item.get('id') !== id))
        localStorageManager.set(LOCAL_STORAGE_KEY, JSON.stringify(newState.toJS()))
        return newState
    }
}, defaultState)

export default reducer
