export const NODE_ENV = process.env.NODE_ENV; // development / production
export const JS_ENV = process.env.JS_ENV; // development / staging / production
export const GITHUB_TOKEN = process.env.GITHUB_TOKEN // token for github API v4
export const API_GRAPHQL_URL = process.env.API_GRAPHQL_URL;
export const OAUTH = process.env.OAUTH;

export const ENVIROMENTS = {
  DEVELOPMENT: 'development',
  STAGING: 'staging',
  PRODUCTION: 'production',
};
