/**
 * Media Breakpoints in pixels
 * @constant
 * @enum {number}
 * @readonly
 * @property {number} xlarge
 * @property {number} large
 * @property {number} medium
 * @property {number} small
 * @property {number} xsmall
 */
export default {
  xlarge: 1920, //modern pc, notebooks, Full HD and above
  large: 1200, //conventional notebooks, pcs, HD TV
  medium: 992, //tablets, small notebooks, Old TV
  small: 767, //mobile phones
  xsmall: 575, //extra small device, old smartphones, IoT, and below.,
}