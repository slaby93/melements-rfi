describe('Search', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.get('[data-automation-id="create-filter-form"]').get('#standard-name').type('test')
    cy.get('[data-automation-id="create-filter-form__add-to-list-button"]').click()
    cy
      .get('[data-automation-id="filter-item"]').first().click()
  })

  it('Checks if we can open chip', () => {
    cy
      .get('[data-automation-id="search-result-item__language-chip"]')
      .should('not.be.visible')

    cy
      .get('[data-automation-id="search-result-item"] [data-automation-id="search-result-item__open-item"]')
      .first()
      .click()

    cy
      .get('[data-automation-id="search-result-item__language-chip"]')
      .should('be.visible')
  })

  it('Checks if we can load more items', () => {
    cy
      .get('[data-automation-id="search-result-item"]').should('have.length', 20)
    cy
      .get('[data-automation-id="search__load-more-button"]').click({ force:true })
    cy
      .get('[data-automation-id="search-result-item"]').should('have.length', 40)

  })
})