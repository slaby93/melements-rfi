describe('Homepage', () => {

  beforeEach(() => {
    cy.visit('/')
  })

  describe('Check filter adding', () => {
    it('should add new filter five times, and remove them using desktop button', () => {
      let numberOfElements = 5

      for (let index = 0; index < numberOfElements; index++) {
        cy.get('[data-automation-id="create-filter-form"]').get('#standard-name').type('test')
        cy.get('[data-automation-id="create-filter-form__add-to-list-button"]').click()
        cy.get('[data-automation-id="filter-item"] ').children().should('have.length', index + 1)
      }

      cy
        .get('[data-automation-id="filter-list-item__delete-button"]')
        .each(item => {
          cy.get('#filter-list ').children().should('have.length', numberOfElements)
          cy.wrap(item).click()
          numberOfElements--
        })

    })

    it('should add new filter five times, and remove them using mobile button', () => {
      cy.viewport('iphone-6')
      let numberOfElements = 5

      for (let index = 0; index < numberOfElements; index++) {
        cy.get('#filter-list ').children().should('have.length', index)
        cy.get('[data-automation-id="create-filter-form"]').get('#standard-name').type('test')
        cy.get('[data-automation-id="create-filter-form__add-to-list-button"]').click()
        cy.get('#filter-list > div').children().should('have.length', index + 1)
      }
      // Actually, we can't check swiping (not yet fully supported) or this would take too much time for 
      // interview task :)

    })
  })

});