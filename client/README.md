## Development
In order to run development mode:

1) > npm install

1) Create `env.development.js` (Create copy of .env.development.js file)

2) > npm run dev

## Production deployment
 Create and publish docker image
> sh ./scripts/create-image.sh
* Run docker image locally
> docker run -p 3000:3000 web-client-service
* Gain access to pm2
> docker exec -it <container-id> pm2 monit

## Other

* If you encounter problem with ENOSPC run
> echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
