module.exports = {
  "roots": [
    "<rootDir>/src"
  ],
  'setupFiles': [
    'raf/polyfill',
  ],
  "modulePaths": [
    '<rootDir>/src',
  ],
  "transform": {
    "^.+\\.tsx?$": "ts-jest"
  },
  "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  "snapshotSerializers": ["enzyme-to-json/serializer"],
  "setupTestFrameworkScriptFile": "<rootDir>/jestSetup.ts",
  "globals": {
    'ts-jest': {
      "diagnostics": false
    }
  },
  "moduleNameMapper": {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.ts',
    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.ts'
  }
}