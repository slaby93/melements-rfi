const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const HappyPack = require('happypack');
const extend = require('util')._extend;
const workboxPlugin = require('workbox-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const TerserPlugin = require('terser-webpack-plugin');

const NODE_ENV = JSON.stringify(process.env.NODE_ENV || 'production');
const JS_ENV = JSON.stringify(process.env.NODE_ENV || 'production');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ENV_DEVELOPMENT = require('./env.development');
const ENV_PRODUCTION = require('./env.production');

console.log('Using NODE_ENV:', NODE_ENV);
console.log('Using JS_ENV:', JS_ENV);

const commonModule = {
  rules: [
    {
      test: /\.(t|j)sx?$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'happypack/loader',
    },
    { test: /(\.css|\.scss|\.sass|\.less)$/, loaders: ['style-loader', 'css-loader', 'sass-loader'] },
    { test: /\.svg/, loader: 'svg-url-loader', options: { stripdeclarations: true } },
  ],
};

const outputPath = './dist';

const commonPlugins = [
  new PreloadWebpackPlugin(),
  new webpack.optimize.ModuleConcatenationPlugin(),
  new CopyWebpackPlugin([
    {
      from: 'src/manifest.json',
      to: './manifest.json',
    },
    {
      from: 'src/resources/icons',
      to: './icons',
    },
    {
      from: 'src/robots.txt',
      to: './robots.txt',
    },
  ]),
  new workboxPlugin.InjectManifest({
    swSrc: './src/workbox-sw.js',
    swDest: 'workbox-sw.js',
  }),
];

const resolve = {
  modules: [
    path.resolve('./src'),
    path.resolve('node_modules'),
  ],
  extensions: ['.ts', '.tsx', '.js', '.jsx'],
  alias: {
    styledComponentsSerilizeHelper: path.resolve(__dirname, 'src/utils/testing/styledComponentsSerilizeHelper.js'),
  },
};

const optimization = {
  minimizer: [new TerserPlugin({
    cache: true,
    parallel: true,
    sourceMap: true,
  })],
  splitChunks: {
    chunks: 'all',
    minSize: 30000,
    maxSize: 0,
    minChunks: 2,
    maxAsyncRequests: 5,
    maxInitialRequests: 5,
    automaticNameDelimiter: '-',
    name: true,
    cacheGroups: {
      vendors: {
        test: /[\\/]node_modules[\\/]/,
        priority: -10,
      },
      assets: {
        test: /[\\/]src[\\/]resources.*/,
        priority: 0,
        chunks: 'all',
      },
      default: {
        minChunks: 2,
        priority: -20,
        reuseExistingChunk: true,
      },
    },
  },
};

const defaultConfig = {
  context: path.join(__dirname, '/'),
  devtool: 'inline-source-map',
  target: 'web',
  mode: NODE_ENV === 'production' ? 'production' : 'development',
  entry: {
    application: [
      '@babel/polyfill',
      'whatwg-fetch',
      './src/index',
    ],
  },
  output: {
    path: path.join(__dirname, outputPath),
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[chunkhash].chunk.js',
    publicPath: '/',
  },
  module: commonModule,
  resolve,
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
    new HappyPack({
      loaders: ['babel-loader'],
      threads: 4,
    }),
    new CopyWebpackPlugin([
      {
        from: 'src/manifest.json',
        to: './manifest.json',
      },
      {
        from: 'src/resources/icons',
        to: './icons',
      },
    ]),
    new webpack.DefinePlugin({
      'process.env': extend({ NODE_ENV, JS_ENV }, ENV_DEVELOPMENT),
    }),
    // new BundleAnalyzerPlugin(),
    ...commonPlugins,
  ],
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
  },
};

const prodConfig = {
  context: path.join(__dirname, '/'),
  devtool: 'inline-source-map',
  mode: 'production',
  entry: {
    application: [
      '@babel/polyfill',
      'whatwg-fetch',
      './src/index',
    ],
  },
  output: defaultConfig.output,
  module: commonModule,
  optimization,
  resolve,
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
    new HappyPack({
      loaders: ['babel-loader'],
      threads: 4,
    }),
    new webpack.DefinePlugin({
      'process.env': extend({ NODE_ENV, JS_ENV }, ENV_PRODUCTION),
    }),
    ...commonPlugins,
  ],
};

module.exports = NODE_ENV === JSON.stringify('production') ? prodConfig : defaultConfig;
