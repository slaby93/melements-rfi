# mElements - RFI task.

Deployed version: https://melements.herokuapp.com

![Image of Cypress](https://i.imgur.com/ncCYPho.png)

## Briefing
All the code starting from `<rootDir>` was created by me. I didn't use any boilerplate genarator.

## How to start
1) Go to `<rootDir>/client` and install packages (`npm install` | `yarn`)
2) Run `npm run dev` 

It will start application at port 3005 ( to not collide with existing services ).

## How to start E2E test
In `client` folder run
> npm run e2e

This will start Cypress GUI which allows you to run specific specs. I have set url to herokuapp so make sure that it's working.

Side note: Due to the fact that I created PWA application, For some reason Cypress have problems with running tests when we have JS files served from service worker cache.

In order to make it run smoothly, open develope tools ( F12 ) and in `Application` tab select `Service Workers` and check both `Update on realod` and `Bypass for network`. You can see that on image I posted on the top of markdown.



## Project structure
In a root folder, we have DockerFile and gulp.
DockerFile is obvious, but why do I use gulp?
Gulp is there to make whole deployment process much easier.

`client` folder contains whole React SPA PWA application. But I needed something to serve my compiled assets, something I want to have full control on. This is why I created `server` folder which contains Typescript powered Koa2 server, it purpose is only to serve files in production in optimized way ( like compressing etc ).

## Problems I encountered:
1) I used Github API v4 ( GraphQL based ), but I discovered that It doesn't have sort funcionality, so this is something I skipped ( my bad I didn't check, it was obvious for me that newer API will have it, but It doesn't ).
2) Unit tests: As we all need to develop ourselfes, I used latest React 16.7 Alpha which introduced React Hooks. When I finished code, I discovered, that Jest/Enzyme doesn't support hooks. But I know that your intention was to check whether or not I can use Enzyme. So I will just link you my another interview task where I used React before Hooks and tested it using enzyme.
https://github.com/slaby93/bidwrangler_demo/tree/master/src

Sample test: https://github.com/slaby93/bidwrangler_demo/blob/master/src/routes/Authorize/Login/__tests__/Login.test.js

3) `The json with selctbox entries should be loaded dynamically upon start of the application` - I had problem understanding this in context of whole application. As for rest of document, it does indicate that I should write SPA app, but I would need to create some REST server for this one ( or am I missing link to API that does server it? ). As this wasn't clear to me what was purpose of this point, I skipped it. In normal enviroment, we would need some REST/GraphQL/Websocket server that would serve such content.