FROM node:10.10-alpine
EXPOSE 3000
WORKDIR /app
ADD . /app
RUN npm install
RUN npm install ts-node typescript -g
RUN ./node_modules/.bin/gulp production:install
RUN ./node_modules/.bin/gulp production:build
CMD ./node_modules/.bin/gulp production:start
