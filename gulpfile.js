const path = require('path')
const gulp = require('gulp')
const util = require('util')
const exec = util.promisify(require('child_process').exec)
const spawn = require('child_process').spawn

gulp.task('client:build', async () => {
		await exec('npm run build', { cwd: path.join(__dirname, './client') })
})

gulp.task('client:install', async () => await new Promise((resolve) => {
	const server = spawn('npm', ['install'], { cwd: path.join(__dirname, './client') })
	server.stdout.on('data', function (data) {
		console.log(data.toString());
	})

	server.stderr.on('data', function (data) {
		console.log('ERROR', data.toString());
	})

	server.on('exit', function (code) {
		console.log('Child process exited with code ' + code.toString());
		resolve()
	})
}))

gulp.task('server:install', async () => await new Promise((resolve) => {
	const server = spawn('npm', ['install'], { cwd: path.join(__dirname, './server') })
	server.stdout.on('data', function (data) {
		console.log(data.toString());
	})

	server.stderr.on('data', function (data) {
		console.log(data.toString());
	})

	server.on('exit', function (code) {
		console.log('Child process exited with code ' + code.toString());
		resolve()
	})
}))

gulp.task('client:clear', async () => {
	await exec('rm -fr client/dist')
})

gulp.task('server:clear', async () => {
	await exec('rm -fr server/public')
})

gulp.task('clear', gulp.parallel('client:clear', 'server:clear'))

gulp.task('general:copyClientToServer', async () => {
	await exec('cp -r client/dist server/public')
})

gulp.task('production:install', gulp.parallel('client:install','server:install'))

gulp.task('production:build', gulp.series('clear','client:build', 'general:copyClientToServer'))

gulp.task('production:start', async () => await new Promise((resolve) => {
	const server = spawn('npm', ['start'], { cwd: path.join(__dirname, './server') })
	server.stdout.on('data', function (data) {
		console.log(data.toString());
	})

	server.stderr.on('data', function (data) {
		console.log(data.toString());
	})

	server.on('exit', function (code) {
		console.log('Child process exited with code ' + code.toString());
		resolve()
	})
}))
